package com.classpath.ekarclientapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
public class EkarClientAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(EkarClientAppApplication.class, args);
	}

}
